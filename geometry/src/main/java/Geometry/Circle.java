package Geometry;

public class Circle implements Shape{

      private double radius;

      public Circle(double radius){
            this.radius = radius;
      }

      public double getRadius(){
            return this.radius;
      }

      @Override
      public double getArea() {
            return Math.PI * Math.pow(this.radius, 2);
      }

      @Override
      public double getPerimeter() {
            return 2 * Math.PI * this.radius;
      }
      
}
