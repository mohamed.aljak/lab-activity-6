package Geometry;

public class LotsOfShapes {
      public static void main(String[] args){
            Shape[] shapes = new Shape[5];
            shapes[0] = new Rectangle(5, 3);
            shapes[1] = new Rectangle(1, 1);
            shapes[2] = new Circle(5);
            shapes[3] = new Circle(1);
            shapes[4] = new Square(5);

            for(Shape shape: shapes){
                  System.out.println(shape.getArea());
                  System.out.println(shape.getPerimeter());
            }
      }
}
