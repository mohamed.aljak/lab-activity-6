package Geometry;

public class Rectangle implements Shape{

      private double length;
      private double width;

      public Rectangle(double length, double width){
            this.length = length;
            this.width =width;
      }

      public double getLength(){
            return this.length;
      }

      public double getWidth(){
            return this.width;
      }

      @Override
      public double getArea() {
            return this.length * this.width;
      }

      @Override
      public double getPerimeter() {
            return this.length * 2 + this.width * 2;
      }
      
}
